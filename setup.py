from setuptools import setup

setup(
    name='protocolo',
    install_requires=[
        'pika==1.2.0',
        'numpy==1.21.1',
        'pandas==1.3.0',
        'pyreadstat==1.1.2',
        'SQLAlchemy==1.4.21',
        'cx-Oracle==8.2.1',
    ],
    packages=[
        'infra',
        'data',
        'domain',
        'infra.config',
        'infra.repositories',
        'infra.adapters',
        'infra.entities',
        'data.models',
        'data.helpers',
        'data.usecases',
        'domain.hepers',
        'domain.usecases',
    ],
)
