import logging.config
import os

import pyreadstat

from domain.hepers import ExceptionDomain

LOGGER = logging.getLogger(__name__)


class ReadFileUseCase:
    """ Read File Class"""

    def __init__(self, path):
        self._path = path

    def set_path(self, path):
        self._path = path

    def reader_file(self):
        if not os.path.exists(self._path):
            LOGGER.error("File not Found, path %s", self._path)
            raise ExceptionDomain("000", "File Not Found")

        if not self._correct_file(self._path):
            LOGGER.error("Type File Incorrect, path %s", self._path)
            raise ExceptionDomain("001", "Type File Incorrect")

        df, meta = pyreadstat.read_sav(self._path)
        return df

    def _correct_file(self, path) -> bool:
        split_tup = os.path.splitext(path)
        if split_tup[1] == ".sav":
            return True
        else:
            return False
