import logging.config
import os
import sys
from sys import platform


def resource_path(relative_path):
    if hasattr(sys, '_MEIPASS'):
        return os.path.join(sys._MEIPASS, relative_path)
    return os.path.join(os.path.dirname(__file__), relative_path)


logging.config.fileConfig(fname=resource_path("logger_config.conf"), disable_existing_loggers=False,
                          defaults={"logfilename": "protocolo.log"})

LOGGER = logging.getLogger(__name__)


def get_resource_ini() -> str:
    LOGGER.debug("RESOURCE PATH %s".format(resource_path("resource.ini")))
    return resource_path("resource.ini")


def get_logger_config() -> str:
    LOGGER.debug("LOGGER PATH %s".format(resource_path("logger_config.conf")))
    return resource_path("logger_config.conf")


def get_name_file(path):
    LOGGER.debug("Get name file of path %s", path)
    array_path_split = None

    if platform == "linux" or platform == "linux2":
        array_path_split = path.split("/")
    elif platform == "darwin":
        array_path_split = path.split("/")
    # OS X
    elif platform == "win32":
        array_path_split = path.split("\\")

    LOGGER.debug("File name is %s", array_path_split)
    return array_path_split[len(array_path_split) - 1]
