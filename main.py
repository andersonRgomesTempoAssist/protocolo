import datetime
import json
import logging.config
import os

import pandas as pd
import pika
import configparser
import functools
import shutil

from domain.usecases import ReadFileUseCase
from domain.usecases import SaveLogInputUseCase
from data.usecases import LogEntradaUseCase
from infra.repositories import LogEntradaRepository
from tools import get_name_file, get_resource_ini

LOGGER = logging.getLogger(__name__)


def step_load_information(body, ch, delivery_tag):
    logging.info("read file and converter to dataframe, %s", body)
    path_file = body['file']
    id_log = body['id_log']
    repository = LogEntradaRepository()
    logger_data_usecases = LogEntradaUseCase(repository=repository)
    logger_usecases = SaveLogInputUseCase(log_input_use_case=logger_data_usecases)
    log_input = logger_usecases.find_by_id(id_log)
    os.chdir(path_file)
    os.chdir('../')
    str_folder = os.getcwd()
    protocol_folder = os.path.join(os.getcwd(), 'protocol')
    file_name = get_name_file(log_input.arquivoorigem)
    if os.path.isdir(protocol_folder) is False:
        LOGGER.debug("Create folder protocol")
        os.mkdir(protocol_folder)
        LOGGER.debug("Copy file on origin to folder protocol")
        shutil.copyfile(log_input.arquivoorigem, protocol_folder + "/" + file_name)
    path_structure_sav = os.path.join(str_folder, 'structure.sav')
    LOGGER.debug("arquivo de origem %s, path_file %s, str_folder %s, path_structure_sav %s",
                 str(os.path.join(protocol_folder, file_name,
                                  path_file, str_folder,
                                  path_structure_sav)))

    step_converter_sav_to_dataframe(protocol_folder + "/" + file_name, path_file, ch, delivery_tag, log_input,
                                    logger_usecases)


def step_converter_sav_to_dataframe(protocol_file, path_sav_processed_file, ch, delivery_tag, input_log_model,
                                    logger_usecases):
    read_file_use_case = ReadFileUseCase(path_sav_processed_file)
    df_file_processed = read_file_use_case.reader_file()
    file_protocol = open(protocol_file, mode="r", encoding='latin-1', errors='ignore')
    string_list = file_protocol.readlines()
    alter_file_processed = open(protocol_file, mode="w", encoding='latin-1')
    for index, row in df_file_processed.iterrows():
        string_list[int(row.FILE_LINE_NUMBER)] = return_message_or_success(string_list, row)
    alter_file_processed.writelines(string_list)
    alter_file_processed.close()
    if 'JOB_TO_EXECUTE' not in df_file_processed.columns:
        df_file_processed['JOB_TO_EXECUTE'] = None

    if df_file_processed['MESSAGE_ERROR'].dtypes == 'float64':
        index = df_file_processed.query("MESSAGE_ERROR.isna() == True").index.values
        df_file_processed["MESSAGE_ERROR"] = df_file_processed["MESSAGE_ERROR"].astype(
            {'MESSAGE_ERROR': str})
        if len(index) > 0:
            for index_ in index:
                df_file_processed.at[index_, 'MESSAGE_ERROR'] = None

    index = df_file_processed.query("MESSAGE_ERROR.str.len() == 0").index.values
    if len(index) > 0:
        for index_ in index:
            df_file_processed.at[index_, 'MESSAGE_ERROR'] = None

    input_log_model = input_log_model._replace(
        regcorretos=((input_log_model.regcorretos if not pd.isnull(input_log_model.regcorretos) else 0) + len(
            df_file_processed.query("MESSAGE_ERROR.isnull() and MESSAGE_ERROR.isna()"))),
        regincorretos=((input_log_model.regincorretos if not pd.isnull(input_log_model.regincorretos) else 0) + len(
            df_file_processed.query("MESSAGE_ERROR.isnull() == False or MESSAGE_ERROR.isna()== False"))),
        regenvinclusao=((input_log_model.regenvinclusao if not pd.isnull(input_log_model.regenvinclusao) else 0) + len(
            df_file_processed.query("MESSAGE_ERROR.isnull() and MESSAGE_ERROR.isna() and OPERACAO == '1'"))),
        regenvalteracao=((input_log_model.regenvalteracao if not pd.isnull(
            input_log_model.regenvalteracao) else 0) + len(
            df_file_processed.query("MESSAGE_ERROR.isnull() and MESSAGE_ERROR.isna() and OPERACAO == '2'"))),
        regenvcancelamento=((input_log_model.regenvcancelamento if not pd.isnull(
            input_log_model.regenvcancelamento) else 0) + len(
            df_file_processed.query("MESSAGE_ERROR.isnull() and MESSAGE_ERROR.isna() and OPERACAO == '3'"))),
        regenvvencido=((input_log_model.regenvvencido if not pd.isnull(
            input_log_model.regenvvencido) else 0) + len(
            df_file_processed.query(
                "~MESSAGE_ERROR.isnull() and ~MESSAGE_ERROR.isna() and MESSAGE_ERROR.str.contains('033')"))),
        data_fim=datetime.date.today(),
        hora_fim=datetime.datetime.now(),
        regjacadastrado=((input_log_model.regjacadastrado if not pd.isnull(
            input_log_model.regjacadastrado) else 0) + len(
            df_file_processed.query(
                "MESSAGE_ERROR.isnull() and MESSAGE_ERROR.isna() and JOB_TO_EXECUTE == 'A' and OPERACAO == '1'"))),
        regduplicadocarga=0
    )
    logger_usecases.save(input_log_model)
    ack_message(ch, delivery_tag)
    LOGGER.info("FINISHED, %s", datetime.datetime.now())


def return_message_or_success(string_list, row):
    if pd.notnull(row.MESSAGE_ERROR):
        return row.MESSAGE_ERROR.ljust(211) + string_list[int(row.FILE_LINE_NUMBER)]
    elif pd.notnull(row.MESSAGE_WARN):
        return row.MESSAGE_WARN.ljust(211) + string_list[int(row.FILE_LINE_NUMBER)]
    else:
        job_execute = '[000][     ]Registro processado com sucesso|'.ljust(211)
        if not string_list.__contains__(job_execute):
            return job_execute + string_list[int(row.FILE_LINE_NUMBER)]
        else:
            return string_list


def ack_message(ch, delivery_tag):
    """Note that `ch` must be the same pika channel instance via which
    the message being ACKed was retrieved (AMQP protocol constraint).
    """
    if ch.is_open:
        ch.basic_ack(delivery_tag)
    else:
        # Channel is already closed, so we can't ACK this message;
        # log and/or do something that makes sense for your app in this case.
        pass


def do_work(conn, ch, delivery_tag, body):
    # Sleeping to simulate 10 seconds of work
    obj = json.loads(body.decode('UTF-8'))
    step_load_information(obj, ch, delivery_tag)


def on_message(ch, method_frame, _header_frame, body, args):
    (conn, callback_consumers) = args
    delivery_tag = method_frame.delivery_tag
    do_work(conn, ch, delivery_tag, body)


if __name__ == '__main__':
    config = configparser.ConfigParser()
    config.read(get_resource_ini())
    host_ = config["queue"]["HOST"]
    user = config["queue"]["USER"]
    password = config["queue"]["PASSWORD"]
    port_ = int(config["queue"]["PORT"])
    credentials = pika.PlainCredentials(user, password)

    parameters = pika.ConnectionParameters(
        host=host_, port=port_, credentials=credentials, heartbeat=0)
    connection = pika.BlockingConnection(parameters)

    channel = connection.channel()
    on_message_callback = functools.partial(on_message, args=(connection, do_work))
    channel.basic_consume('process.protocol', on_message_callback)

    try:
        channel.start_consuming()
    except KeyboardInterrupt:
        channel.stop_consuming()

    connection.close()
