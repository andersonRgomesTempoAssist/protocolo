import logging.config

import sqlalchemy
from sqlalchemy.exc import NoResultFound

from data.models import InputLogModel
from infra.config import DBConnectionHandler
from infra.entities.log_entrada import LogEntradaEntity

LOGGER = logging.getLogger(__name__)


class LogEntradaRepository:
    """Class LogEntrada Repository"""

    def find_by_id(self, id_seqlog):
        with DBConnectionHandler() as db_connection:
            try:
                record = db_connection.session.query(LogEntradaEntity).get(id_seqlog)
                return InputLogModel(
                    id_seqlog=record.id_seqlog,
                    id_clientecorporativo=record.id_clientecorporativo,
                    id_contrato=record.id_contrato,
                    data=record.data,
                    id_tipocarteira=record.id_tipocarteira,
                    hora=record.hora,
                    arquivoorigem=record.arquivoorigem,
                    totalregistros=record.totalregistros,
                    regcorretos=record.regcorretos,
                    regincorretos=record.regincorretos,
                    regenvinclusao=record.regenvinclusao,
                    regenvalteracao=record.regenvalteracao,
                    regenvcancelamento=record.regenvcancelamento,
                    regenvreativacao=record.regenvreativacao,
                    regenvprecadastro=record.regenvprecadastro,
                    regenverro=record.regenverro,
                    regenvfatal1=record.regenvfatal1,
                    regenvfatal2=record.regenvfatal2,
                    regenvvencido=record.regenvvencido,
                    regenvlote=record.regenvlote,
                    data_fim=record.data_fim,
                    hora_fim=record.hora_fim,
                    status=record.status,
                    id_estrutura=record.id_estrutura,
                    regjacadastrado=record.regjacadastrado,
                    regnaoprocessado=record.regnaoprocessado,
                    regignorado=record.regignorado,
                    regduplicadocarga=record.regduplicadocarga)
            except sqlalchemy.exc.OperationalError as error:
                LOGGER.error(error)
                raise
            except NoResultFound as error:
                LOGGER.error(error)
                return None
            except Exception as error:
                LOGGER.error(error)
                raise
            finally:
                db_connection.session.close()

    def find_register(self, id_corporate_client, id_contract, id_wallet_type, id_struct, name_file_origin):
        with DBConnectionHandler() as db_connection:
            try:
                entity = db_connection.session.query(LogEntradaEntity).filter(
                    (LogEntradaEntity.id_clientecorporativo == id_corporate_client) &
                    (LogEntradaEntity.id_contrato == id_contract) &
                    (LogEntradaEntity.id_tipocarteira == id_wallet_type) &
                    (LogEntradaEntity.id_estrutura == id_struct) &
                    (LogEntradaEntity.arquivoorigem.like("%" + name_file_origin + "%"))).one()

                return entity.id_seqlog
            except sqlalchemy.exc.OperationalError as error:
                self.db_connection.throw(error)
            except NoResultFound as error:
                return None
            except Exception as error:
                self.db_connection.throw(error)
            finally:
                db_connection.session.close()

    def update(self, entity: LogEntradaEntity):
        with DBConnectionHandler() as db_connection:
            try:
                db_connection.session.merge(entity)
                db_connection.session.commit()
                return entity.id_seqlog
            except sqlalchemy.exc.OperationalError as error:
                LOGGER.error(error)
                db_connection.session.rollback()
                raise
            except Exception as error:
                LOGGER.error(error)
                db_connection.session.rollback()
                raise
            finally:
                db_connection.session.close()

    def insert(self, entity: LogEntradaEntity):
        if entity.id_seqlog is not None:
            return self.update(entity)
        else:
            with DBConnectionHandler() as db_connection:
                try:
                    db_connection.session.add(entity)
                    db_connection.session.commit()
                    return entity.id_seqlog
                except sqlalchemy.exc.OperationalError as error:
                    LOGGER.error(error)
                    db_connection.session.rollback()
                    raise
                except Exception as error:
                    LOGGER.error(error)
                    db_connection.session.rollback()
                    raise
                finally:
                    db_connection.session.close()
