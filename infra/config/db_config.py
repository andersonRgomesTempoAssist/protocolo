import configparser
import os
from sys import platform

import cx_Oracle
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker

from tools import get_resource_ini


class DBConnectionHandler:
    """Sqlalchemy database connection"""

    def __init__(self):
        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        self.__connection_string = config["database"]["STRING_CONNECTION"]
        self.session = None
        try:
            if cx_Oracle.clientversion() is None:
                cx_Oracle.init_oracle_client(lib_dir=config["database"]["LIB_DIR"])
        except Exception as error:
            if platform == "win32":
                cx_Oracle.init_oracle_client(lib_dir=config["database"]["LIB_DIR"])
                return
            raise

    def get_engine(self):
        """Return connection Engine
        :param - None
        :return - engine connection to Database
        """
        engine = create_engine(self.__connection_string, coerce_to_unicode=True)
        return engine

    def __enter__(self):
        engine = create_engine(self.__connection_string)
        session_maker = sessionmaker()
        self.session = session_maker(bind=engine)
        return self

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.session.close()  # pylint: disable=no-member

    @staticmethod
    def __session__():
        """Static Method"""
        with DBConnectionHandler() as db_connection:
            try:
                yield db_connection.session
            except:
                db_connection.session.rollback()
                raise
            finally:
                db_connection.session.close()
