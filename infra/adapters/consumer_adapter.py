# pylint: disable=C0111,C0103,R0205
import pika
import configparser
import functools

import main
from tools import get_resource_ini


class ConsumerAdapter(object):

    def ack_message(self, ch, delivery_tag):
        """Note that `ch` must be the same pika channel instance via which
        the message being ACKed was retrieved (AMQP protocol constraint).
        """
        if ch.is_open:
            ch.basic_ack(delivery_tag)
        else:
            # Channel is already closed, so we can't ACK this message;
            # log and/or do something that makes sense for your app in this case.
            pass

    def on_message(self, ch, method_frame, _header_frame, body, args):
        (conn, callback_consumers) = args
        delivery_tag = method_frame.delivery_tag
        main.do_work(conn, ch, delivery_tag, body)

    def __init__(self, callback_consumers):
        config = configparser.ConfigParser()
        config.read(get_resource_ini())
        host_ = config["queue"]["HOST"]
        user = config["queue"]["USER"]
        password = config["queue"]["PASSWORD"]
        port_ = int(config["queue"]["PORT"])
        credentials = pika.PlainCredentials(user, password)

        parameters = pika.ConnectionParameters(
            host=host_, port=port_, credentials=credentials, heartbeat=5)
        connection = pika.BlockingConnection(parameters)

        channel = connection.channel()
        on_message_callback = functools.partial(self.on_message, args=(connection, callback_consumers))
        channel.basic_consume('process.protocol', on_message_callback)

        try:
            channel.start_consuming()
        except KeyboardInterrupt:
            channel.stop_consuming()

        connection.close()
